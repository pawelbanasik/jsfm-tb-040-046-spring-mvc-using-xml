package com.pawelbansik.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.pawelbanasik.service.demo.GenericWelcomeService;

@Controller
public class WelcomeController {

	// Autowiring - helps Spring automatically satisfy a beans dependency by finding
	// other beans in the application context or other spring configuration that
	// matches the beans dependency requirement
	@Autowired
	private GenericWelcomeService genericWelcomeService;

	@RequestMapping("/")
	public String doWelcome(Model model) {

		
		List<String> welcomeMessage = genericWelcomeService.getWelcomeMessage("Pawel Banasik");

		model.addAttribute("myWelcomeMessage", welcomeMessage);

		return "welcomeNew"; 
	}
}
